package eg.edu.alexu.csd.ds.hangman.test.cs10;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import eg.edu.alexu.csd.ds.hangman.IHangman;
import eg.edu.alexu.csd.ds.hangman.csd10.HangmanDummyEngine;

public class cs10 {


	
	@Test
	public void test2() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr=new String[1];
		arr[0]="x";
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('x');
		Assert.assertEquals("x", result);
	}
	
	
	@Test
	public void test3() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"xyzx"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('x');
 	}
	
	
	@Test
	public void test4() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"xyzx"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('a');
		String result1 =hangman.guess('a');
		String result2 =hangman.guess('a');
		String result3 =hangman.guess('a');
		String result4 =hangman.guess('a');
		Assert.assertEquals(null, result4);
	}
	
	
	@Test
	public void test5() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"ayzx"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('a');
		String result1 =hangman.guess('a');
		String result2 =hangman.guess('a');
		String result3 =hangman.guess('a');
		String result4 =hangman.guess('a');
		Assert.assertEquals("a---", result4);
	}
	
	
	@Test
	public void test6() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"ayzx"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('a');
		String result1 =hangman.guess('y');
		String result2 =hangman.guess('z');
		String result3 =hangman.guess('z');
		String result4 =hangman.guess(' ');
		Assert.assertEquals("ayz-", result4);
	}
	
	
	@Test
	public void test7() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"ayzx"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess(' ');
		String result1 =hangman.guess(' ');
		String result2 =hangman.guess(' ');
		String result3 =hangman.guess(' ');
		String result4 =hangman.guess(' ');
		Assert.assertEquals(null, result4);
	}
	
	
	@Test
	public void test8() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"   "};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess(' ');
		Assert.assertEquals("   ", result);
	}
	
	
	@Test
	public void test9() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"abcda"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess('a');
		String result1 =hangman.guess('b');
		String result2 =hangman.guess('c');
		String result3=hangman.guess('d');
		Assert.assertEquals("abcda", result3);
	}
	
	
	@Test
	public void test10() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr={"abcda"};
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		hangman.setMaxWrongGuesses(5);
		String result =hangman.guess(null);
		String result1 =hangman.guess(null);
		String result2 =hangman.guess(null);
		String result3 =hangman.guess(null);
		String result4 =hangman.guess(null);
		Assert.assertEquals("-----", result4);
	}
	
	@Test
	public void test11() {
		HangmanDummyEngine hangman = new HangmanDummyEngine();
		String []arr=new String [2];
		arr[0]="abc";
		hangman.setDictionary(arr);
		String secretWord=hangman.selectRandomSecretWord();
		Assert.assertEquals("abc", secretWord);
	}
	@Test
	//test1
	public void SetDectionarytest() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		secretword= Hangman.selectRandomSecretWord();
		assertEquals("CANADA", secretword);
		
		
		//SUCCESS IN FIRST TEST 
		//CAN READ CORRECTLY
		
	}
	@Test
	public void guesstest1() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();

		String guess;
		guess=Hangman.guess('C');
		assertEquals("C-----", guess);

		//SUCCESS IN 2ND TEST
		//GUESS FUNCTION RUNS CORRECTLY


}
	@Test
	//test3
	public void guesstest2() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		guess=Hangman.guess('A');
		assertEquals("-A-A-A", guess);
		
		//SUCCESS IN 3RD TEST
		//REPEATION IS AVALIBLE HERE
		
		
}
	@Test
	//test4
	public void guesstest3() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		guess=Hangman.guess('A');
		assertEquals("-A-A-A", guess);
		
		//ERORR IN 4TH TEST
		//CHANGING FROM LOWER CASE TO UPPER CASE WAS MISSED
		//THIS ERROR IS HANDLED IN MAIN FUCTION NOT ON GUESS FUNCTION
		
		
}
	
	@Test
	//test5
	public void guesstest4() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(0);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		guess=Hangman.guess('Z');
		assertEquals(null, guess);
		
		//WRONG IN 5TH TEST
		//EXPECTED NULL FOUND DASHES
		//ERROR OCURES BECAUE OF WORNGMAX==0 BUT IT SHOULD BE WORNGMAX<=0
		
		
}
	@Test
	//test6
	public void guesstest5() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		for(int i=0;i<4;i++)Hangman.guess('Z');
		guess=Hangman.guess('Z');
		
		assertEquals(null, guess);
		
		//SUCCESS IN 6TH TEST
		//AFTER ENTERING MAXWRONG ANSWERS RETURN NULL		
}
	
	@Test
	//test7
	public void guesstest6() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		
		guess=Hangman.guess('C');
		guess=Hangman.guess('C');
		
		assertEquals("C-----", guess);
		
		//SUCCESS IN 7TH TEST
		//REPAETING RIGHT ANSWER HAVE NO CHANGE		
}
	@Test
	//test8
	public void guesstest7() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={"CANADA"};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		for(int i=0;i<4;i++)Hangman.guess('/');
		guess=Hangman.guess('/');
		
		assertEquals(null, guess);
		
		//SUCCESS IN 8TH TEST
		//CONSIDERED BACKSLASH AND COMMAS AS WRONG GUSSES
}
	@Test
	//test11
	public void guesstest10() {
		HangmanDummyEngine Hangman= new HangmanDummyEngine ();
		String[] words={""};
		String secretword;
		Hangman.setDictionary(words);
		Hangman.setMaxWrongGuesses(5);
		secretword= Hangman.selectRandomSecretWord();
		
		String guess;
		
		
		guess=Hangman.guess('Z');
		
		assertEquals(null, guess);
		
		//ERROR
		//EMPTY LIST IS ACCEPTABLE 
		//I DON'T KNOW IF HE SOULD HANDLE THIS CASE OR NOT
}
	
}
