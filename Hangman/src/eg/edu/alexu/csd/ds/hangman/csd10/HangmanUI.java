package eg.edu.alexu.csd.ds.hangman.csd10;
// AHMED EID ABDEL MONEAM MOHAMED     10
//ASSIGNMENT 1  HANGMAN GAME  
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import eg.edu.alexu.csd.ds.hangman.IHangman;



public class HangmanUI {
	public static String[] words = new String[1000];
	static int i=0;
	static String readFile(String fileName) throws IOException {
	   
	   try{
		   BufferedReader br = new BufferedReader(new FileReader(fileName));
		   try {
			   StringBuilder sb = new StringBuilder();
			   String line = br.readLine();
			   while (line != null) {
				   words[i++]=line;
				   sb.append(line);
				   sb.append(",");
				   line = br.readLine();
			   }
			   return sb.toString();
		   } finally {
			   br.close();
		   }
	   }catch(FileNotFoundException  e){
		   System.out.println("Error File Not Found ");
		   System.out.println("Generating the Random Dictionary");
		   words[0]="EGYPT";
		   words[1]="SPAIN";
		   words[2]="FRANCE";
		   words[3]="ITALY";
		   return null;
	   }
	}
	public static void main(String[] args) throws IOException {
		IHangman hangman = new HangmanDummyEngine();
		//readFile("a.a");   //READ FROM FILE .....FILE NAME (a.a)
		words[0]="EGYPT";
		hangman.setDictionary(words);		//SET DICTIONARY 
		hangman.setMaxWrongGuesses(5);		
		String secret = hangman.selectRandomSecretWord();		//SELECT RANDOM WORD TO PLAY WITH IT !
		Scanner input = new Scanner(System.in);
		Character guess = null;
		do{
			String result = hangman.guess(guess);
			if(result==null){
				System.out.println("Fail! correct answer = '" + secret + "'");  //GAME OVER
				return;
			}
			System.out.println(result);
			if(!result.contains("-")){	
				System.out.println("Well Done!");				//WINNER
				return;
			}
			guess = input.next().toUpperCase().charAt(0);			//SCANNING THE CHAR FROM USER
		}while(true);
	}
}