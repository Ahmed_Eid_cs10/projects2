package eg.edu.alexu.csd.ds.hangman.csd10;

import eg.edu.alexu.csd.ds.hangman.IHangman;

import java.lang.reflect.Array;
import java.util.Random;
import java.util.Scanner;
public class HangmanDummyEngine implements IHangman{
	String[] dictionary = new String[1000];				//ARRAY TO SET DICTIONARTY
	String word;										//RANDOM WORD 
	int length=1000,counter;
	char[] hiddenword;				//HIDDEN WORD (USER WORD)
	Boolean []check=new Boolean[length];				//CHECK IF CHAR VISITED OR NOT
	@Override
	public void setDictionary(String[] words) {			//FUNCTION TO SET ARRAY FROM FILE
		 dictionary = words;
	}
	@Override
	public String selectRandomSecretWord() {			//GENERATING RANDOM WORD TO PLAY WITH!
		do{
			word= dictionary[new Random ().nextInt(dictionary.length)];		//GENERATING RANDOM INDEX
		}while(word==null);
		
		length=word.length();
		hiddenword=new char [length];
		for(int i=0;i<length;i++){						//SET HIDDEN WORD WITH "-"
            hiddenword[i]='-';
            check[i]=false;
        }
		return word;
	}

	@Override
	public String guess(Character c) {			//FUNCTION TO CHECK IF USER's CHAR IS VALID OR NOT
		if(c!=null){
			int index=word.indexOf(c);
			if(word.contains(c+"")){
				if(!check[index]){
					while(index>=0){
						hiddenword[index]=c;
						check[index]=true;
						index=word.indexOf(c,index+1);
					}
				}
			
			}else{
				counter--;
			}
			if(counter>0){
				String str=new String (hiddenword);
				return str;
			}else{
				return null;
			}
		}
		else{
			String str=new String (hiddenword);
			return str;
		}
	}
	
	@Override
	public void setMaxWrongGuesses(Integer max) { 			//MAX NUMBER OF WRONG
		// TODO Auto-generated method stub
		counter=max;
	}


}