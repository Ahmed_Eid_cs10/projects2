package JUnit;
//////////////////////////////////////////////////////////////////////////////
/////// NAME : AHMED EID ABDEL MONEAM 								//////////
///////  ID : 10 													//////////
///////////////////////////////////////////////////////////////////////////////
import static org.junit.Assert.*;

import java.awt.Point;

import junit.framework.Assert;
import eg.edu.alexu.csd.ds.iceHockey.IPlayersFinder;
import eg.edu.alexu.csd.ds.iceHockey.cs10.HockyDummyEngine;

public class Test {

	@org.junit.Test
	public void test1() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"33JUBU33",
				"3U3O4433",
				"O33P44NB",
				"PO3NSDP3",
				"VNDSD333",
				"OINFD33X"
				};
		Point [] result =hocky.findPlayers(map, 3, 16);
		Point[] expected ={ new Point(4,5),new Point(13, 9),new Point(14 ,2)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test2() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
					"44444H44S4",
					"K444K4L444",
					"4LJ44T44XH",
					"444O4VIF44",
					"44C4D4U444",
					"4V4Y4KB4M4",
					"G4W4HP4O4W",
					"4444ZDQ4S4",
					"4BR4Y4A444",
					"4G4V4T4444"
				};
		Point [] result =hocky.findPlayers(map,4, 16);
		Point[] expected ={ new Point(3,8),new Point(4,16),new Point(5 ,4),new Point (16,3),new Point (16,17),new Point(17,9)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test3() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"8D88888J8L8E888",
				"88NKMG8N8E8JI88",
				"888NS8EU88HN8EO",
				"LUQ888A8TH8OIH8",
				"888QJ88R8SG88TY",
				"88ZQV88B88OUZ8O",
				"FQ88WF8Q8GG88B8",
				"8S888HGSB8FT8S8",
				"8MX88D88888T8K8",
				"8S8A88MGVDG8XK8",
				"M88S8B8I8M88J8N",
				"8W88X88ZT8KA8I8",
				"88SQGB8I8J88W88",
				"U88H8NI8CZB88B8",
				"8PK8H8T8888TQR8"
				};
		Point [] result =hocky.findPlayers(map,8, 9);
		Point[] expected ={new Point (1, 17), new Point(3, 3),new Point (3, 10),new Point (3, 25), new Point(5, 21),new Point (8, 17),new Point (9, 2),new Point (10,9),new Point (12,23),new Point (17,16),new Point (18,3),
				new Point(18,11),new Point (18,28),new Point (22,20),new Point(23,26),new Point (24,15),new Point (27,2),new Point (28,26),new Point (29,16)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test4() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"11111",
				"1AAA1",
				"1A1A1",
				"1AAA1",
				"11111"
				};
		Point [] result =hocky.findPlayers(map,1, 3);
		Point[] expected ={ new Point(5,5),new Point(5,5)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test5() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"33JUBU33",
				"3U3O4433",
				"O33P44NB",
				"PO3NSDP3",
				"VNDSD333",
				"OINFD33X"
				};
		Point [] result =hocky.findPlayers(map, 3, 16);
		Point[] expected ={ new Point(4,5),new Point(13, 9),new Point(14 ,2)};
		assertNotSame(result, expected);
	}
	
	@org.junit.Test
	public void test6() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"33JUBU33",
				"3U3O4433",
				"O33P44NB",
				"PO3NSDP3",
				"VNDSD333",
				"OINFD33X"
				};
		Point [] result =hocky.findPlayers(map, 9, 16);
		Point[] expected ={};
		assertArrayEquals(result, expected);
	}
	
	
	@org.junit.Test
	public void test7() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"33333",
				"33333",
				"33333",
				"33333",
				};
		Point [] result =hocky.findPlayers(map, 3, 16);
		Point[] expected ={ new Point(5,4)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test8() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"3303",
				"3333",
				"1333",
				"1533",
				};
		Point [] result =hocky.findPlayers(map,3,32);
		Point[] expected ={ new Point(4,4)};
		assertArrayEquals(result, expected);
	}

	
	@org.junit.Test
	public void test9() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"3003",
				"0000",
				"0000",
				"3003",
				};
		Point [] result =hocky.findPlayers(map,3,4);
		Point[] expected ={ new Point(1,1),new Point(1,7),new Point(7,1),new Point (7,7)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test10() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"1100",
				"0110",
				"0011",
				"0001",
				};
		Point [] result =hocky.findPlayers(map,1,4);
		Point[] expected ={ new Point(4,4)};
		assertArrayEquals(result, expected);
	}
	@org.junit.Test
	public void test11() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333",
				"3333"
				};
		Point [] result =hocky.findPlayers(map,3,4);
		Point[] expected ={ new Point(4,50)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test12() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={""};		
		Point [] result =hocky.findPlayers(map,1,4);
		Point[] expected ={};
		assertArrayEquals(result, expected);
	}
	@org.junit.Test
	public void test13() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={"1"};		
		Point [] result =hocky.findPlayers(map,1,4);
		Point[] expected ={new Point (1,1)};
		assertArrayEquals(result, expected);
	}
	@org.junit.Test
	public void test14() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={"1"};		
		Point [] result =hocky.findPlayers(map,1,4);
		Point[] expected ={new Point (1,1)};
		assertArrayEquals(result, expected);
	}
	@org.junit.Test
	public void test15() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={"3333",
						"3113",
						"3113",
						"3333"
		};		
		Point [] result =hocky.findPlayers(map,3,12);
		Point[] expected ={new Point (4,4)};
		assertArrayEquals(result, expected);
	}
	
	@org.junit.Test
	public void test16() {
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={"3111",
						"3111",
						"3111",
						"3333"
		};		
		Point [] result =hocky.findPlayers(map,3,12);
		Point[] expected ={new Point (4,4)};
		assertArrayEquals(result, expected);
	}
	@org.junit.Test
	public void test17() {
		IPlayersFinder test =new HockyDummyEngine();
		
			
			Point [] arr=new Point[2];
			arr[0] = new Point(5,5);
			arr[1] = new Point(5,5);		
				String[] photo={
					"11111",
					"1AAA1",
					"1A1A1",
					"1AAA1",
					"11111"
					};
				
				int team =1;
				int threshold = 3;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
	@org.junit.Test

	public void test18() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			Point [] arr=new Point[1];
			arr[0] = new Point(5,5);		
				String[] photo={
					"44444",
					"4AAA4",
					"4AAA4",
					"4AAA4",
					"44444"
					};
				
				int team =4;
				int threshold = 16;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////	
	@org.junit.Test

	public void test19() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			Point [] arr=new Point[6];
			arr[0] = new Point(1,1);
			arr[1] = new Point(3,3);
			arr[2] = new Point(5,5);
			arr[3] = new Point(7,7);
			arr[4] = new Point(9,9);
			arr[5] = new Point(11,11);
				String[] photo={
					"9BBBBB",
					"B9BBBB",
					"BB9BBB",
					"BBB9BB",
					"BBBB9B",
					"BBBBB9",
					};
				
				int team =9;
				int threshold = 2;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////	
	@org.junit.Test

	public void test20() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			Point [] arr=new Point[1];
			arr[0] = new Point(6,6);
			
				String[] photo={
					"999999",
					"999999",
					"999999",
					"999999",
					"999999",
					"999999",
					};
				
				int team =9;
				int threshold = 2;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////
	@org.junit.Test

	public void test21() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			Point [] arr=new Point[1];
			arr[0] = new Point(5,5);
			
				String[] photo={
					"11111",
					"1S1S1",
					"1S1S1",
					"1S1S1",
					"11111"
					};
				
				int team =1;
				int threshold = 16;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////
	@org.junit.Test

	public void test22() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			Point [] arr=new Point[1];
			arr[0] = new Point(5,8);
			
				String[] photo={
					"1X1X1",
					"X1X1X",
					"1X1X1",
					"X1X1X",
					"11111"
					};
				
				int team =1;
				int threshold = 16;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////
	@org.junit.Test

	public void test23() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			
			
				String[] photo={
					"1X1X1",
					"X1X1X",
					"1X1X1",
					"X1X1X",
					"11111"
					};
				
				int team =3;
				int threshold = 16;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			Point [] arr = new Point[0]; 
			assertArrayEquals(cord, arr);
	}
///////////////////////////////////////////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////
	@org.junit.Test
	public void test24() {
		IPlayersFinder test =new HockyDummyEngine();

		
			
			
			
				String[] photo=null;
				
				int team =3;
				int threshold = 16;

			Point [] cord =  test.findPlayers(photo,team,threshold);
			assertArrayEquals(cord, null);
	}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
	@org.junit.Test

public void test25() {
	IPlayersFinder test =new HockyDummyEngine();
	
		
		
		
			String[] photo={
					"4AVBN4",
					"4V44G4",
					"A4ABBS"
			};
			
			int team =3;
			int threshold = 16;

		Point [] cord =  test.findPlayers(photo,team,threshold);
		Point [] arr = new Point[0];
		assertArrayEquals(cord,arr);
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
	@org.junit.Test
public void test26() {
	IPlayersFinder test =new HockyDummyEngine();
	
		
		
		
			String[] photo={
					"44444H44S4",
					"K444K4L444",
					"4LJ44T44XH",
					"444O4VIF44",
					"44C4D4U444",
					"4V4Y4KB4M4",
					"G4W4HP4O4W",
					"4444ZDQ4S4",
					"4BR4Y4A444",
					"4G4V4T4444"
					};
			
			int team =4;
			int threshold = 16;

		Point [] cord =  test.findPlayers(photo,team,threshold);
		Point [] arr = new Point[6];
		arr[0] = new Point(3,8);arr[1] = new Point(4,16);arr[2] = new Point(5,4);arr[3] = new Point(16,3);arr[4] = new Point(16,17);arr[5] = new Point(17,9);
		
		assertArrayEquals(cord,arr);
}
}
