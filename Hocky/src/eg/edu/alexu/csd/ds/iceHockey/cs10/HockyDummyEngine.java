package eg.edu.alexu.csd.ds.iceHockey.cs10;
//////////////////////////////////////////////////////////////////////////////
/////// NAME : AHMED EID ABDEL MONEAM 								//////////
///////  ID : 10 													//////////
///////////////////////////////////////////////////////////////////////////////
import java.awt.Point;
import java.util.Arrays;
import java.util.Comparator;

import eg.edu.alexu.csd.ds.iceHockey.IPlayersFinder;

public class HockyDummyEngine implements IPlayersFinder{
	char [][] plane ;
	int lengR,lengC,counter=0,min_x=5000,max_x=-1,min_y=500,max_y=-1,length=0;
	@Override
	public Point[] findPlayers(String[] photo, int team, int threshold) {
		// TODO Auto-generated method stub
		 lengR = photo.length;							//to get length of array
	     lengC = photo[0].length();						//to get length of words					
	    plane = new char[lengR][lengC];					//creating array
		Point [] points=new Point [100];				//creating array of points of centers
														//of groups
	    for(int i=0; i<lengR;i++)
	    {
	        plane[i] = photo[i].toCharArray();			//converting array of strings to array of 2D char
	    }
		for(int i=0;i<lengR;i++){
			for(int j=0;j<lengC;j++){
				if(plane[i][j]-'0'==team){
					recursive((char)team,i,j);			//calling recursive function
					if(counter>=threshold){
						points[length]=new Point(((min_x*2+max_x*2)/2)+1,((min_y*2+max_y*2)/2)+1);
						length++;
					}
					
					min_x=5000;							//restore the counter 
					max_x=0;
					min_y=5000;
					max_y=0;
					counter=0;
				}
			}
			
		}
		
		Point [] result =new Point[length];
		for(int i=0;i<length;i++){
			result[i]=points[i];
		}
		Arrays.sort(result, new Comparator<Point>() {			//sorting the array of points
		    public int compare(Point point1, Point point2) {
		        int x = Integer.compare(point1.x, point2.x);
		        if(x == 0)
		            return Integer.compare(point1.y, point2.y);
		        else
		            return x;
		    }
		});
		
		return result;
	}
	
	void recursive (char target,int x,int y){
		if(plane[x][y]-'0'!=target){
			return;
		}
		counter+=4;
		plane[x][y]='-';
		if(y<=min_x){				//to get the boundaries of box 
			min_x=y;
		}
		if(y>=max_x){
			max_x=y;
		}
		if(x<=min_y){
			min_y=x;
		}
		if(x>=max_y){
			max_y=x;
		}
		
		if(y<lengC-1){				//recursive in 4 D 
			recursive(target,x,y+1);
		}
		if(x<lengR-1){
			recursive(target,x+1,y);
		}
		if(y!=0){
			recursive(target,x,y-1);
		}
		if(x!=0){
		recursive(target,x-1,y);
		}	
	}
}
