package eg.edu.alexu.csd.ds.iceHockey.cs10;

import java.awt.Point;

import eg.edu.alexu.csd.ds.iceHockey.IPlayersFinder;
//////////////////////////////////////////////////////////////////////////////
/////// NAME : AHMED EID ABDEL MONEAM 								//////////
///////  ID : 10 													//////////
///////////////////////////////////////////////////////////////////////////////
public class HockyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IPlayersFinder hocky= new HockyDummyEngine();
		String [] map={
				"33JUBU33",
				"3U3O4433",
				"O33P44NB",
				"PO3NSDP3",
				"VNDSD333",
				"OINFD33X"
				};
		Point [] output;
		output=hocky.findPlayers(map, 3,16);
		System.out.print("{");
		for(int i = 0; i < output.length ; ++i){
			System.out.printf(" (%d,%d)",output[i].x , output[i].y);
			if(i != output.length-1)
					System.out.print(",");
		}
		System.out.println("}");
		}
	}


