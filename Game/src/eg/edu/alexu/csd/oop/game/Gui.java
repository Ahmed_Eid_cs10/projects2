package eg.edu.alexu.csd.oop.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import eg.edu.alexu.csd.oop.game.GameEngine.GameController;

public class Gui extends JFrame {

	private static final long serialVersionUID = 1L;
	private JButton newGame, load;
	private final JMenuBar menubar;
	private JMenu about;
	private JMenuItem abt;

	public Gui() {
		this.setVisible(true);
		this.setSize(400, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Game");
		menubar = new JMenuBar();
		about = new JMenu("About");
		abt = new JMenuItem("Team Mates");
		this.setJMenuBar(menubar);
		menubar.add(about);
		about.add(abt);
		newGame = new JButton("New Game");
		load = new JButton("Load");

		Dimension dim = new Dimension(175, 50);
		newGame.setPreferredSize(dim);
		load.setPreferredSize(dim);
		Font font = new Font("SansSerif", Font.BOLD, 25);
		newGame.setFont(font);
		load.setFont(font);

		setLayout(new BorderLayout());
		setContentPane(new JLabel(new ImageIcon(getClass().getClassLoader().getResource("background.jpg"))));
		setLayout(new FlowLayout());
		add(newGame);
		add(load);
		// Just for refresh :) Not optional!
		setSize(399, 399);
		setSize(400, 400);

		ButtonsListener b = new ButtonsListener();
		abt.addActionListener(b);
		newGame.addActionListener(b);
		load.addActionListener(b);
		validate();

	}

	private class ButtonsListener implements ActionListener {

		private MyActionListener actionL = new MyActionListener();

		@Override
		public void actionPerformed(ActionEvent e) {

			String x = ((JButton) e.getSource()).getText();
			actionL.performAction(x);

		}
	}
}