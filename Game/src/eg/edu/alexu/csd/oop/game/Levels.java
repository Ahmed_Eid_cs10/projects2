package eg.edu.alexu.csd.oop.game;

public class Levels implements ILevels, Observer {


	private static Levels newLevels = null;
	private LevelsPlugin levels = new LevelsPlugin();
	public static Levels getInstance() { // Singelton Pattern
		if (newLevels == null) {
			newLevels = new Levels();

		}
		return newLevels;
	}

	@Override
	public void setSpeed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setControlSpeed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setMovableObjects() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setControlableObjects() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setConstantObjects() {
		// TODO Auto-generated method stub

	}



	public void notifyObserver(int score) {
		// TODO Auto-generated method stub

		if (score == 0) {
			// Easy level = new Easy ();
			levels.levelsFactory("eg.edu.alexu.csd.oop.game.Easy");

		} else if (score == 1) {
			// Medium level = new Medium();
			levels.levelsFactory("eg.edu.alexu.csd.oop.game.Medium");
		} else if (score == 2) {
			// Hard level = new Hard();
			levels.levelsFactory("eg.edu.alexu.csd.oop.game.Hard");
		}
	}

}
