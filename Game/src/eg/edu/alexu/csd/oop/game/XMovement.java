package eg.edu.alexu.csd.oop.game;

public class XMovement implements IStates{
	private static XMovement newInstance=null;
	private int direction;
	private WorldImplementation world =  WorldImplementation.getInstance(0,0);
	
	private Shape currentConstant ;
	private Shape currentMoving ;
	
	public static XMovement getInstance(){	//Singelton Pattern 
		if(newInstance==null){
			newInstance=new XMovement();
			
		}
		newInstance.run();
		return newInstance;
	}
	public void run(){
		updateObject();
		
	}

	
	public void updateObject(){
		currentConstant = world.getCurrentConstantObject();
		currentMoving = world.getCurrentMovingObject();
		if(!currentConstant.isLeft()){direction=-1;}else direction=1;
		currentMoving.setX(currentMoving.getX()+direction*2);
		setCurrentMovingObject();
	}
	public void setCurrentMovingObject(){
		world.setCurrentMovingObject(currentMoving);
		
	}
	
	

}
