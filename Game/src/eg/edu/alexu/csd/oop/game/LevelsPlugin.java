package eg.edu.alexu.csd.oop.game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LevelsPlugin {
	
	@SuppressWarnings("unchecked")
	private static final Logger logger = LoggerFactory.getLogger(LevelsPlugin.class);	

	public List <Class < ? extends Levels>> loadLevels() {
		 
		  List<Class < ? extends Levels>> classes = new ArrayList<Class < ? extends Levels>>();
		  LinkedList<JarFile> jarList = new LinkedList<JarFile>();
		  String string = System.getProperty("java.class.path");
		  String[] strar = string.split(File.pathSeparator);
		  
		  for (int i = 1; i < strar.length; i++) {	  
			  if(strar[i].contains(".jar") && !strar[i].contains("log4j")){
			try {
				jarList.add(new JarFile(strar[i]));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				
				logger.error("Error occuring while levels plugin ");
				e1.printStackTrace();
			}
			  }
			  
		}
	 
		for(int j = 0 ; j < jarList.size() ; j++){
		Enumeration<?> e = jarList.get(j).entries();
		while (e.hasMoreElements()) {
	        JarEntry je = (JarEntry) e.nextElement();
	        if(je.isDirectory() || !je.getName().endsWith(".class")){
	            continue;
	        }
	    // -6 because of .class
	    String className = je.getName().substring(0,je.getName().length()-6);
	    className = className.replace('/', '.');
	    try {
			Class<?> cls = Class.forName(className);
			if(Levels.class.isAssignableFrom(cls))
			{
				classes.add((Class<? extends Levels>) cls);
			}
		} catch (ClassNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		} 
		 try {
			 jarList.get(j).close();
	        } catch (IOException e2) {
	            // TODO Auto-generated catch block
	            e2.printStackTrace();
	        }
		
		}
		
		return classes;
	}
	
public Levels levelsFactory ( String s) {
		
		Levels temp = null;
		
		for (int i = 0; i < loadLevels().size(); i++) {
			
			try {
				if (loadLevels().get(i) == Class.forName(s)) {
					 temp = (Levels) loadLevels().get(i).newInstance();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return temp;
}

}
