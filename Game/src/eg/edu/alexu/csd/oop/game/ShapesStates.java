package eg.edu.alexu.csd.oop.game;

public class ShapesStates {

	private IStates state;

	public void chooseJob(boolean movingHorizontal) {
		if (movingHorizontal) {
			state = XMovement.getInstance();
		} else {
			state = YMovement.getInstance();

		}
	}

}
