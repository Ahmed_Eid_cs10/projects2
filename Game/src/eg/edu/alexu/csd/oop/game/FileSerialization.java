package eg.edu.alexu.csd.oop.game;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileSerialization {
	private static final Logger logger = LoggerFactory.getLogger(FileSerialization.class);

	public void serialize(SnapShot snapShot) {
		try {

			// write object to file
			FileOutputStream fos = new FileOutputStream("game.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(snapShot);

			logger.info("SAved");
			oos.close();
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("Object cant serialization ");
			logger.info("Termenating porject");
			e.printStackTrace();
		}

	}

	public SnapShot deserialize() {
		SnapShot shot = null;
		try {
			// read object from file
			FileInputStream fis = new FileInputStream("game.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			shot = (SnapShot) ois.readObject();

			ois.close();
		} catch (FileNotFoundException e) {
			logger.error("File not found esception ! ! ");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			logger.error("Class not found exception ");
			e.printStackTrace();
		}
		return shot;
	}

}