package eg.edu.alexu.csd.oop.game;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import eg.edu.alexu.csd.oop.game.GameEngine.GameController;


public class MyActionListener {
		
	private SnapShot shot;
	public void performAction(String s) {
		if (s.equals("New Game")) {

			WorldImplementation x = WorldImplementation.getInstance(1400, 900);
			MenuBar menu = new MenuBar();
			final GameController gameController = GameEngine.start("Mcdonalds", x, menu.getMenubar(), Color.BLACK);
			/*menu.getPause().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					gameController.pause();
				}
			});
			menu.getResume().addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					gameController.resume();
				}
			});*/
		} else if (s.equals("Load")) {
			SavingStrategy savingStrategy = new SavingStrategy();
			shot = savingStrategy.load();
			// set level,score,controlled objects to the game before call
			// get instance of world
			WorldImplementation x = WorldImplementation.getInstance(1400, 900);
			x.setContrlableObjects(shot.getControl());
			x.setScore(shot.getScore());
			MenuBar menu = new MenuBar();
			GameEngine.start("Game", x, menu.getMenubar(), JFrame.EXIT_ON_CLOSE);
		} else if (s.equals("abt")) {
			JOptionPane.showMessageDialog(null, "Ahmed Eid - Aliaa Othman - Radwa Elmasry - Shehab Elsamany",
					"Our Team", JOptionPane.INFORMATION_MESSAGE);
		}
}

}
