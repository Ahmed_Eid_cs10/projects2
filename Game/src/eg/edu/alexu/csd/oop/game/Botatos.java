package eg.edu.alexu.csd.oop.game;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;
import java.util.Stack;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.GameObjectImpl;
import eg.edu.alexu.csd.oop.game.IPic;
import eg.edu.alexu.csd.oop.game.WorldImplementation;

public class Botatos  extends GameObjectImpl implements IPic{
	private BufferedImage[] spriteImages= getSpriteImages();
	private WorldImplementation world = WorldImplementation.getInstance(0, 0);
	private Map images=world.getImages();
	@Override
	public void draw() {
		try {
			this.setPath("/botatos.png");
			if(images.get("botatos")!=null)
				spriteImages[0]=(BufferedImage) images.get("botatos");
			else {
				spriteImages[0] = ImageIO.read(getClass().getResourceAsStream(this.getPath()));
				images.put("botatos",spriteImages[0] );
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setClown(false);
		this.setHeight(spriteImages[0].getHeight());
		this.setWidth(spriteImages[0].getWidth());
	}
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.getClass().getSimpleName();
	}
	@Override
	public void setRightHand(Stack x) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setLeftHand(Stack x) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Stack getRightHand() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Stack getLeftHand() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void addRight(GameObject x) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addLeft(GameObject x) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void removeRight() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void removeLeft() {
		// TODO Auto-generated method stub
		
	}

}
