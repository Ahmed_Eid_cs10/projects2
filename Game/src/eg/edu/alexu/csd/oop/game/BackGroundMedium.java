package eg.edu.alexu.csd.oop.game;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Stack;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.GameObjectImpl;
import eg.edu.alexu.csd.oop.game.IPic;

public class BackGroundMedium extends GameObjectImpl implements IPic {
	private BufferedImage[] spriteImages = getSpriteImages();

	public BackGroundMedium() {

		try {
			Image img = ImageIO.read(getClass().getResourceAsStream("/back2.jpg"));
			img = img.getScaledInstance(1400, 900, Image.SCALE_SMOOTH);
			spriteImages[0] = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
			Graphics2D graphics = spriteImages[0].createGraphics();
			graphics.drawImage(img, 0, 0, null);
			graphics.setStroke(new BasicStroke());
			graphics.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void draw() {

		setClown(false);
		this.setHeight(spriteImages[0].getHeight());
		this.setWidth(spriteImages[0].getWidth());
		this.setX(0);
		this.setY(0);

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRightHand(Stack x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLeftHand(Stack x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Stack getRightHand() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stack getLeftHand() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addRight(GameObject x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addLeft(GameObject x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeRight() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeLeft() {
		// TODO Auto-generated method stub
		
	}

}
